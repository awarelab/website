---
title: Spinning Up in Deep RL - TF2 implementation
summary: This repository includes the Spinning Up in Deep RL algorithms ports to TensorFlow v2.
tags:
- Deep Learning
date: "2020-12-15T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: "https://github.com/awarelab/spinningup_tf2"

#image:
#  caption: Photo by rawpixel on Unsplash
#  focal_point: Smart
#
#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

