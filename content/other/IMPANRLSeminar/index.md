---
title: IMPAN RL Seminar
summary: RL Seminar at IM PAN led by Łukasz Kuciński and Piotr Miłoś.
tags:
- Reinforcement Learning
- Deep Learning
- Machine Learning
date: "2018-11-05T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: "https://www.impan.pl/en/activities/seminars/reinforcement-learning"

# image:
#   caption: Photo by rawpixel on Unsplash
#   focal_point: Smart
# 
# links:
# - icon: twitter
#   icon_pack: fab
#   name: Follow
#   url: 
# url_code: ""
# url_pdf: ""
# url_slides: ""
# url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---
