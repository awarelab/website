---
title: RL Course
summary: RL course led by Łukasz Kuciński and Piotr Miłoś.
tags:
- Deep Learning
date: "2020-03-23T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: "https://sites.google.com/view/uczeniezewzmocnieniem/home-en"

#image:
#  caption: Photo by rawpixel on Unsplash
#  focal_point: Smart

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---

Reinforcement learning (RL) is one of the pillars of contemporary AI research. It focuses on finding optimal strategies in sequential decision-making problems. It is often convenient to think about RL environments as games. A player (called an agent) interacts with the environment by taking actions, and the consequences of these actions (such as win or loss) are deferred in time. Successes in 'solving' games like go, Dota2, StarCraftII are impressive examples of applying RL techniques.
