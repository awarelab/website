---
title: NeurIPS 2021 Deep RL Workshop Accepted Paper
summary: "
We are delighted to inform you that our paper *Continuous Control With Ensemble
Deep Deterministic Policy Gradients* has been selected at the NeurIPS 2021 Deep RL Workshop!

The work was done in collaboration with Google Brain Zurich, Nomagic.ai, and the
University of Warsaw. Congratulations to all the authors!"
date: "2021-10-23T00:00:00Z"


# Optional external URL for project (replaces project detail page).
external_link: "https://sites.google.com/view/deep-rl-workshop-neurips2021"

# image:
#   caption: Photo by rawpixel on Unsplash
#   focal_point: Smart
# 
# links:
# - icon: twitter
#   icon_pack: fab
#   name: Follow
#   url: 
# url_code: ""
# url_pdf: ""
# url_slides: ""
# url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---
