---
title: AAMAS 2022 Accepted Paper
summary: "We are pleased to inform that the paper *Off-Policy Correction For
Multi-Agent Reinforcement Learning* was accepted at AAMAS 2022. The work was
done in collaboration with the University of Warsaw and Google Brain Zurich.
Congratulations to all the authors!"
date: "2021-12-19T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: "publication/2021-zawalski-marl/"
# external_link: "https://aamas2022-conference.auckland.ac.nz/"

---
