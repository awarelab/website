---
title: NeurIPS 2021 Accepted Papers
summary: "Great news, three papers accepted at NeurIPS 2021:

- Subgoal Search For Complex Reasoning Tasks

- Continual World: A Robotic Benchmark For Continual Reinforcement Learning

- Catalytic Role Of Noise And Necessity Of Inductive Biases In The Emergence Of Compositional Communication


The work was done in collaboration with Deepmind, University of Toronto, and
Jagiellonian University. Congratulations to all the authors!"
date: "2021-09-28T00:00:00Z"



#RL Seminar at IM PAN led by Łukasz Kuciński and Piotr Miłoś.
#tags:
#- Reinforcement Learning
#- Deep Learning
#- Machine Learning
#date: "2018-11-05T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: "https://docs.google.com/presentation/d/14WiRXLIwdw3mxK-XknitZdC-9ql_Er2r6UGO0O_ebSI/edit?usp=sharing"

# image:
#   caption: Photo by rawpixel on Unsplash
#   focal_point: Smart
# 
# links:
# - icon: twitter
#   icon_pack: fab
#   name: Follow
#   url: 
# url_code: ""
# url_pdf: ""
# url_slides: ""
# url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---
