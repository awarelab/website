---
title: "Trust, but verify: Alleviating Pessimistic Errors in Model-Based Exploration"
date: 2021-02-01
authors: ["Konrad Czechowski", "Tomasz Odrzygóźdź", "Michał Izworski", "Marek Zbysiński", "Łukasz Kuciński", "Piotr Miłoś"]
publication_types: ["1"]
abstract: "We propose trust-but-verify (TBV) mechanism, a new method which uses model uncertainty estimates to guide exploration. The mechanism augments graph search planning algorithms with the capacity to deal with learned model’s imperfections. We  identify  certain  type  of  frequent  model  errors,  which  we  dub false loops,and  which  are  particularly  dangerous  for  graph  search  algorithms  in  discrete environments. These errors impose falsely pessimistic expectations and thus hinder exploration. We confirm this experimentally and show that TBV can effectively alleviate them. TBV combined with MCTS or Best First Search forms an effective model-based reinforcement learning solution, which is able to robustly solve sparse reward problems."
featured: false
publication: "IJCNN 2021"
url_pdf: "https://confcats-event-sessions.s3.amazonaws.com/ijcnn21/papers/N-1627.pdf"
tags: ["model-based", "rl", "uncertainty", "planning", "ensembles", "mcts"]
---

