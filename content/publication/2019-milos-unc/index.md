---
title: "Uncertainty-sensitive Learning and Planning with Ensembles"
date: 2019-01-01
publishDate: 2020-06-20T12:54:20.206682Z
authors: ["Piotr Miłoś", "Łukasz Kuciński", "Konrad Czechowski", "Piotr Kozakowski", "Maciek Klimek"]
publication_types: ["2"]
abstract: "We propose a reinforcement learning framework for discrete environments in which an agent makes both strategic and tactical decisions. The former manifests itself through the use of value function, while the latter is powered by a tree search planner. These tools complement each other. The planning module performs a local \textit{what-if} analysis, which allows to avoid tactical pitfalls and boost backups of the value function. The value function, being global in nature, compensates for inherent locality of the planner. In order to further solidify this synergy, we introduce an exploration mechanism with two distinctive components: uncertainty modelling and risk measurement. To model the uncertainty we use value function ensembles, and to reflect risk we use propose several functionals that summarize the implied by the ensemble. We show that our method performs well on hard exploration environments: Deep-sea, toy Montezuma's Revenge, and Sokoban. In all the cases, we obtain speed-up in learning and boost in performance."
featured: false
publication: "ICML 2020 Uncertainty & Robustness in Deep Learning Workshop"
url_pdf: "https://arxiv.org/pdf/1912.09996.pdf"
tags: ["model-based", "rl", "uncertainty", "planning", "ensembles", "mcts", "sokoban"]
---

