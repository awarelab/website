---
title: "Robust Planning using Adaptive Entropy Tree Search"
date: 2021-05-26
authors: ["Piotr Kozakowski", "Mikołaj Pacek", "Piotr Miłoś"]
publication_types: ["1"]
abstract: "
We present the Adaptive Entropy Tree Search (ANTS) algorithm, which builds on recent successes of maximum entropy planning [Xiao et al., 2019, Dam et al., 2020] while mitigating their arguably major practical drawback - sensitivity to the temperature setting. ANTS stabilizes entropy within the search tree by dynamically managing temperature. This combined with several other improvements results in a hyperparameter-robust behavior across a diverse set of tasks, as confirmed experimentally with high scores on the Atari benchmark. Moreover, we demonstrate that ANTS is a capable component of a planning-learning loop akin to Silver et al. [2018]. Finally, we theoretically show that temperature adaptation retains exponential convergence in the softmax bandit setting, similar to Xiao et al. [2019]. We believe these features make ANTS a well-founded, practical choice for a planner in complex tasks."
featured: false
publication: "submitted"
tags: ["rl", "planning", "mcts", "maxentropy"]
---

