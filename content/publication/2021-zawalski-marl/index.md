---
title: "Off-Policy Correction For Multi-Agent Reinforcement Learning"
date: 2021-05-26
authors: ["Michał Zawalski", "Błażej Osiński", "Henryk Michalewski", "Piotr Miłoś"]
publication_types: ["1"]
abstract: " Multi-agent reinforcement learning (MARL) provides a framework for
problems involving multiple interacting agents.  Despite apparent similarity to
the single-agent case, multi-agent problems are often harder to train and
analyze theoretically. In this work, we propose a new actor-critic algorithm –
MA-Trace, which follows the paradigm of centralized training and decentralized
execution. Our algorithm, that extends V-Trace to the MARL setting, utilizes
importance sampling as an off-policy correction method. This allows for highly
scalable multi-worker (multi-node) training. Importantly, MA-Trace enjoys
theoretical grounding - we prove a fixed-point theorem that guarantees
convergence. We evaluate the algorithm extensively on the StarCraft Multi-Agent
Challenge set - a standard benchmark for multi-agent algorithms. MA-Trace,
despite its simplicity, achieves high performance on all the tasks and exceeds
state-of-the-art results on some of them."
featured: false
publication: "AAMAS 2022"
url_pdf: "https://arxiv.org/abs/2111.11229"
url_project: "https://sites.google.com/view/ma-trace"
tags: ["multi-agent", "MA-Trace", "importance sampling"]
---

