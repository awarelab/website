---
title: "Catalytic Role Of Noise And Necessity Of Inductive Biases In The Emergence Of Compositional Communication"
date: 2021-05-26
authors: ["Łukasz Kuciński", "Tomasz Korbak", "Paweł Kołodziej", "Piotr Miłoś"]
publication_types: ["1"]
abstract: "Communication is compositional if complex signals can be represented as a combination of simpler subparts. In this paper, we theoretically show that inductive biases on both the training framework and the data are needed to develop a compositional communication. Moreover, we prove that compositionality spontaneously arises in the signaling games, where agents communicate over a noisy channel. We experi- mentally confirm that a range of noise levels, which depends on the model and the data, indeed promotes compositionality. Finally, we provide a comprehensive study of this dependence and report results in terms of recently studied compositionality metrics: topographical similarity, conflict count, and context independence."
featured: false
publication: "NeurIPS 2021"
url_pdf: "https://arxiv.org/abs/2111.06464"
tags: ["compositionality", "signaling games", "noisy channel", "deep learning"]
---

