---
title: "Simulation-based reinforcement learning for real-world autonomous driving"
date: 2020-01-01
publishDate: 2020-06-20T12:54:20.206948Z
authors: ["Błażej Osiński", "Adam Jakubowski", "Paweł Zięcina", "Piotr Miłoś", "Christopher Galias", "Silviu Homoceanu", "Henryk Michalewski"]
publication_types: ["1"]
abstract: "We use synthetic data and a reinforcement learning algorithm to train a driving system controlling a full-size real-world vehicle in a number of restricted driving scenarios. The driving policy uses RGB images as input. We show how design decisions about perception, control and training impact the real-world performance."
featured: false
url_pdf: "https://arxiv.org/pdf/1911.12905.pdf"
publication: "ICRA 2020"
tags: ["sim2real", "autonomous driving", "reinforcement learning"]
---

[Project webpage](https://sites.google.com/view/sim2realautonomousdriving/home)