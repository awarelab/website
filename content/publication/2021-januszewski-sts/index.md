---
title: "Structure and Randomness in Planning and Reinforcement Learning"
date: 2021-01-01
publishDate: 2020-06-20T12:54:20.206948Z
authors: ["Konrad Czechowski", "Piotr Januszewski", "Piotr Kozakowski", "Łukasz Kuciński", "Piotr Miłoś"]
publication_types: ["1"]
abstract: "Planning in large state spaces inevitably needs to balance depth and breadth of the search. It has a crucial impact on planners performance and most manage this interplay implicitly. We present a novel method Shoot Tree Search (STS), which makes it possible to control this trade-off more explicitly. Our algorithm can be understood as an interpolation between two celebrated search mechanisms: MCTS and random shooting. It also lets the user control the bias-variance trade-off, akin to TD(n), but in the tree search context.In experiments on challenging domains, we show that STS can get the best of both worlds consistently achieving higher scores."
featured: false
publication: "IJCNN 2021"
tags: ["model-based", "rl", "mcts", "shooting", "planning", "football", "video"]
---

