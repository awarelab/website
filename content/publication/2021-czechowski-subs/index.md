---
title: "Subgoal Search For Complex Reasoning Tasks"
date: 2021-05-26
authors: ["Konrad Czechowski", "Tomasz Odrzygóźdź", "Marek Zbysiński", "Michał
Zawalski", "Krzysztof Olejnik", "Yuhuai Wu", "Łukasz Kuciński", "Piotr Miłoś"]
publication_types: ["1"]
abstract: "Humans excel in solving complex reasoning tasks through a mental
process of moving from one idea to a related one.  Inspired by this, we propose
Subgoal Search (kSubS) method.  Its key component is a learned subgoal
generator, an analog of human intuition, that produces a diversity of subgoals
that are both achievable and closer to the solution.  Using subgoals reduces the
search space and induces a high-level search graph suitable for efficient
planning. In this paper, we implement kSubS using a transformer-based subgoal
module coupled with the classical best-first search framework. We show that a
simple approach of generating k-the step ahead subgoals is surprisingly
efficient on three challenging domains: two popular puzzle games,  Sokoban and
the Rubik’s Cube,  and an inequality proving benchmark INT. kSubS achieves
strong results including state-of-the-art on INT within a modest computational
budget."
featured: false
publication: "NeurIPS 2021"
url_pdf: "https://arxiv.org/abs/2108.11204"
url_project: "https://sites.google.com/view/subgoal-search"
tags: ["search", "deep learning", "hierarchical planning"]
---

