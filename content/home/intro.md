+++
# A Demo section created with the Blank widget.
# Any elements can be added in the body: https://sourcethemes.com/academic/docs/writing-markdown-latex/
# Add more sections by duplicating this file and customizing to your requirements.

widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 15  # Order that this section will appear.

title = ""
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"

  # Background gradient.
  gradient_start = "DarkGreen"
  gradient_end = "ForestGreen"

  # Background image.
  image = "background7.png"  # Name of image in `static/img/`.
  image_darken = 0.7  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  image_size = "actual"  #  Options are `cover` (default), `contain`, or `actual` size.
  image_position = "top"  # Options include `left`, `center` (default), or `right`.
  image_parallax = true  # Use a fun parallax-like fixed background effect? true/false

  # Text color (true=light or false=dark).
  text_color_light = true

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["20px", "0", "20px", "0"]

[advanced]
 # Custom CSS.
 css_style = "text-shadow: 1px 1px 2px black;"

 # CSS class.
 css_class = ""
+++
<h1>AWARElab</h1>
<p><em style="color: #f8f8f2; font-size:1.4rem;">AI Warsaw Research lab</em></p>

<p style="color: #f8f8f2; font-size:1.2rem;text-align: justify">
AWARElab is a machine learning group established in 2018 as part of the
Institute of Mathematics of the Polish Academy of Sciences and based in
Warsaw, Poland. The lab's mission is to advance the field of Artificial
Intelligence by designing algorithms that allow machines to learn and
automatically acquire skills to solve hard problems.
