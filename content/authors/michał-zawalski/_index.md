---
# Display name
title: Michał Zawalski

# Username (this should match the folder name)
authors:
- michał-zawalski
weight: 25

# Is this the primary user of the site?
superuser: true

# Role/position
role: Ph.D. Student

# Organizations/Affiliations
organizations:
- name: University of Warsaw
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: My research interests include combinatorial games and multi-agent RL.

interests:
- Reinforcement Learning
- Competitive programming
- Contract bridge

education:
  courses:
  - course: Master in Computer Science
    institution: University of Warsaw
    year: 2020
  - course: Bachelor in Mathematics
    institution: University of Warsaw
    year: 2020

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:mz371908@students.mimuw.edu.pl"
- icon: github
  icon_pack: fab
  link: https://github.com/do-not-be-hasty
- icon: cv
  icon_pack: ai
  link: https://students.mimuw.edu.pl/~mz371908/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Ph.D. Students
---



I am a Ph.D. student of Computer Science at the Warsaw Doctoral School of
Mathematics and Computer Science, jointly run by the [University of
Warsaw](https://www.uw.edu.pl/) and the [Institute of Mathematics of the Polish
Academy of Sciences](https://www.impan.pl/). My advisor is [prof. Piotr Miłoś]({{% ref "/content/authors/piotr-miłos/_index.md" %}}).

My Master of Computer Science thesis deals with solving the Rubik's Cube with minimal assumptions, which is an intractable task for standard RL algorithms. In my Ph.D. I focus on topics of planning and multi-agent reinforcement learning. 

I gained work experience during internships in Google Zurich and Samsung Warsaw.

I enjoy solving algorithmic problems and coding contests. I have successfully participated in the Central European Regional Contest, Google Hashcode, Microsoft Bubble Cup, Polish Collegiate Programming Contest, and others.
