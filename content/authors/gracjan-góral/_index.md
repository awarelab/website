---
# Display name
title: Gracjan Góral

# Username (this should match the folder name)
authors:
- gracjan-goral
weight: 5

# Is this the primary user of the site?
superuser: true

# Role/position
role: Ph.D. student

# Organizations/Affiliations
organizations:
 - name: University of Warsaw
   url: ""

# Short bio (displayed in user profile at end of posts)
bio: TODO

interests:
- Machine Learning
- Reinforcement Learning

education:
  courses:
  - course: PhD in Computer Science
    institution: University of Warsaw
    year: in progress
  - course: MSc in Mathematics
    institution: University of Wroclaw
    year: 2019
  - course: BSc in Mathematics
    institution: University of Wroclaw
    year: 2017

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: mailto:gracjan.goral.b@gmail.com

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Ph.D. Students
---



I am a Ph.D. sudent at [University of Warsaw](https://en.uw.edu.pl/), advised by
[prof. Piotr
Miłoś](https://awarelab.gitlab.io/website/authors/piotr-mi%C5%82os/) and [dr.
Łukasz
Kuciński](https://awarelab.gitlab.io/website/authors/%C5%82ukasz-kucinski/). I am interested in Reinforcment Learning, Machine Learning and the application of the theory of control in Reinforcment Learning.

