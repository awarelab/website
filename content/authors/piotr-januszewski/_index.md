---
# Display name
title: Piotr Januszewski

# Username (this should match the folder name)
authors:
- piotr-januszewski
weight: 10

# Is this the primary user of the site?
superuser: true

# Role/position
role: "Ph.D. student"

# Organizations/Affiliations
organizations:
 - name: Gdansk University of Technology
   url: ""
 - name: University of Warsaw
   url: ""

# Short bio (displayed in user profile at end of posts)
bio: “Be ready. Work. Hard. Enjoy it! It fits every situation." ~ Chris Hadfield

interests:
- Reinforcement Learning
- High Performance Computing
- Visual Astronomy

education:
  courses:
  - course: Ph.D. in Computer Science
    institution: Gdansk University of Technology
    year: 2019
  - course: Master in Computer Science
    institution: Gdansk University of Technology
    year: 2018
  - course: Engineer in Computer Science
    institution: Gdansk University of Technology
    year: 2014

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: user
  icon_pack: fas
  link: https://piojanu.github.io/
- icon: envelope
  icon_pack: fas
  link: mailto:piotr.januszewski@pg.edu.pl
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/piojanu/
- icon: github
  icon_pack: fab
  link: https://github.com/piojanu
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.co.uk/citations?user=NfsQfqgAAAAJ

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Ph.D. Students

---

I am a Ph.D. student advised jointly by prof. Paweł Czarnul Gdańsk (University of Technology (GUT)) and [prof. Piotr Miłoś]({{% ref "/content/authors/piotr-miłos/_index.md" %}}) (Polish Academy of Sciences). I work in the field of reinforcement learning.

Earlier, I was a co-founder and the first president of the Student Research Group Gradient at the GUT, taught Data Science at infoShare Academy, and dealt with face detection and emotion analysis at Quantum.CX. I have experience in general computer engineering and high-performance computing, which I gained during an internship at Intel Poland.

<!--- TODO(PM): It would be good to add CV (see my profile how to do this).---->

