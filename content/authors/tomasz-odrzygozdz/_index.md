---
# Display name
title: Tomasz Odrzygóźdź

# Username (this should match the folder name)
authors:
- tomasz-odrzygozdz

# Is this the primary user of the site?
superuser: true

# Role/position
role: Postdoctoral researcher

# Organizations/Affiliations
organizations:
- name: University of Warsaw
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: My research interests include distributed robotics, mobile computing and programmable matter.

interests:
- Machine learning
- Reinforcement Learning
- Quantum mechanics

education:
  courses:
  - course: Postdoc in AI
    institution: University of Warsaw
    year: 2020
  - course: Ph.D. in Mathematics
    institution: Polish Academy of Sciences
    year: 2019
  - course: Master in Mathematics
    institution: University of Warsaw
    year: 2013

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:tomaszo@mimuw.edu.pl"
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=J2ERJ7cAAAAJ&hl=en
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/tomasz-odrzygozdz/

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
---



I am a postdoctoral researcher at the [University of Warsaw](https://www.mimuw.edu.pl/en). Currently, I have been working in the field of  reinforcement learning, focusing on topics related to planning.

I obtained my Ph.D. in 2019 (at the Polish Academy of Sciences cooperating with the McGill University).  My thesis was in the area of geometric group theory.  During Ph.D., I researched random groups and quantum relativistic theory of information.

I mentor gifted high-school and university students in mathematics, physics and AI, as a part of cooperation with [Adamed SmartUp](https://adamedsmartup.pl/en/) and [Akademeia](https://akademeia.edu.pl/en/).

<!--- TODO(PM): It would be good to add CV (see my profile how to do this). --->

