---
# Display name
title: Piotr Kozakowski

# Username (this should match the folder name)
authors:
- piotr-kozakowski
weight: 15

# Is this the primary user of the site?
superuser: true

# Role/position
role: Ph.D. Student

# Organizations/Affiliations
organizations:
- name: University of Warsaw
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: I like to pick up new things in various places and apply them in seemingly unrelated places. It makes my life interesting!

interests:
- Reinforcement Learning
- Deep Learning
- Functional Programming

education:
  courses:
  - course: Ph.D. in Computer Science
    institution: University of Warsaw
    year: in progress
  - course: MSc in Computer Science
    institution: University of Warsaw
    year: 2018
  - course: BSc in Computer Science
    institution: University of Warsaw
    year: 2016

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:p.kozakowski@mimuw.edu.pl'  # For a direct email link, use "mailto:test@example.org".
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=QkYiDCwAAAAJ
- icon: github
  icon_pack: fab
  link: https://github.com/koz4k
- icon: medium
  icon_pack: fab
  link: https://medium.com/@koz4k
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Ph.D. Students

# Highlight the author in author lists? (true/false)
highlight_name: false
---
I am a Ph.D. student at the [University of Warsaw](https://www.uw.edu.pl/), advised by [prof. Piotr Miłoś]({{% ref "/content/authors/piotr-miłos/_index.md" %}}). I am interested in reinforcement learning, particularly model-based and offline RL.

I am the Principal Investigator of a research grant funded by the National Science Centre - [Thinking Far Ahead: Long-horizon planning using deep model-based reinforcement learning](https://www.ncn.gov.pl/sites/default/files/listy-rankingowe/2020-03-16pfoa/streszczenia/486548-en.pdf).

In addition to RL, I have general experience in deep learning, gained by working at [Google](https://google.com) and [Lyrebird.ai](https://lyrebird.ai), and pursuing various projects in my spare time. I have worked on visual object detection and classification, image and text clustering, ranking, language modeling, voice and music synthesis and classification, and time series prediction.

I am one of the authors of [TRAX](https://github.com/google/trax), a Google framework for cutting-edge deep learning and reinforcement learning research.

I coordinated the workshop sessions at [PL in ML 2018](https://conference2018.mlinpl.org/) and served as a teaching assistant at [EEML 2020](https://www.eeml.eu/) and workshop tutor at [OxML 2020](https://www.oxfordml.school/oxml2020). 
