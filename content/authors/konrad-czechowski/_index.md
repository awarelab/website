---
# Display name
title: Konrad Czechowski

# Username (this should match the folder name)
authors:
- konrad-czechowski
weight: 1

# Is this the primary user of the site?
superuser: true

# Role/position
role: Ph.D. Student

# Organizations/Affiliations
organizations:
- name: University of Warsaw
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: ""

interests:
- Reinforcement Learning
- Deep Learning

education:
  courses:
  - course: Ph.D. in Computer Science
    institution: University of Warsaw
    year: in progress
  - course: MSc in Mathematics
    institution: University of Warsaw
    year: 2017
  - course: BSc in Mathematics
    institution: University of Warsaw
    year: 2015

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: 'mailto:k.czechowski@mimuw.edu.pl'  # For a direct email link, use "mailto:test@example.org".
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=ni7tRv4AAAAJ
# - icon: github
#   icon_pack: fab
#   link: https://github.com/koz4k
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Ph.D. Students
---

I am a Ph.D. student advised by [prof. Piotr Miłoś]({{% ref "/content/authors/piotr-miłos/_index.md" %}}). 
My main research interest is deep learning and its applications to sequential decision making.
I have helped to organize the first two editions of [PL in ML](https://conference2018.mlinpl.org/) conference (now know as [ML in PL](https://mlinpl.org/) ).

Prior to my Ph.D. I worked for [deepsense.ai](https://deepsense.ai/) as Data Scientist and 
participated with success in several [kaggle competitions](https://www.kaggle.com/kczechowski).

<!--- TODO[PM]: would be good to make little longer.TODO(PM): It would be good to add CV (see my profile how to do this).--->
