---
# Display name
title: Piotr Miłoś

# Username (this should match the folder name)
authors:
- piotr-miłos
weight: 1

# Is this the primary user of the site?
superuser: true

# Role/position
role: Principal Investigator

# Organizations/Affiliations
organizations:
- name: Polish Academy of Sciences
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: My research interests include distributed robotics, mobile computing and programmable matter.

interests:
- Machine learning
- Reinforcement Learning
- Probability Theory

education:
  courses:
  - course: Ph.D. in Probability Theory
    institution: Polish Academy of Sciences
    year: 2008
  - course: Master in Computer Science
    institution: University of Warsaw
    year: 2005
  - course: Master in Mathematics
    institution: University of Warsaw
    year: 2004

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:pmilos@mimuw.edu.pl"
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.co.uk/citations?user=Se68XecAAAAJ
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/piotr-milos-4b02151/
- icon: home
  icon_pack: fas
  link: http://www.mimuw.edu.pl/~pmilos
- icon: cv
  icon_pack: ai
  link: https://www.mimuw.edu.pl/~pmilos/cv-pmilos.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
---

An associate professor at the [Institute of Mathematics, Polish Academy of Sciences](https://www.impan.pl/en).

I have been working in machine learning focusing on *reinforcement learning*. I believe RL methods may evolve to deliver general decision making algorithms, which would transform the world.

I am the PI of a RL research grant founded by the National Science Center. I am a co-host of the [reinforcement learning seminar](https://www.mimuw.edu.pl/~pmilos/#seminar) and coorganize [reinforcement learning course](https://www.mimuw.edu.pl/~pmilos/#teaching). I have a strong background in probability theory and stochastic modeling. 


Contact me, if you are interested in Ph.D./post-doc positions. I encourage prospective master's students to undertake a [research project](https://www.mimuw.edu.pl/~pmilos/#teaching).

