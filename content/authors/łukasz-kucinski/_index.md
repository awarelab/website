---
# Display name
title: Łukasz Kuciński

# Username (this should match the folder name)
authors:
- łukasz-kucinski

weight: 2

# Is this the primary user of the site?
superuser: true

# Role/position
role: Principal Investigator

# Organizations/Affiliations
organizations:
- name: Polish Academy of Sciences
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: My research interests include reinforcement learning, decision-making under uncertainty and optimization.

interests:
- Machine Learning
- Reinforcement Learning
- Optimization

education:
  courses:
  - course: Ph.D. in Mathematics (intersection of game theory, optimal control, and stochastic processes)
    institution: Polish Academy of Sciences
    year: 2015
  - course: Master in Mathematics
    institution: University of Warsaw
    year: 2004

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:lkucinski@impan.pl"
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=l6dK-VUAAAAJ

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Researchers
---

I am an Assitant Professor at the [Institute of Mathematics of the Polish Academy of Sciences](www.impan.pl/en).
I focus on machine learning, reinforcement learning, and optimization.
In particular, I am interested in designing algorithms that
allow artificial agents to develop complex behaviors and autonomously learn
to acquire decision-making and problem-solving skills.
I am a co-host of the [reinforcement learning
seminar](https://www.impan.pl/pl/dzialalnosc/seminaria/uczenie-ze-wzmocnieniem)
and co-organizer of the [reinforcement learning
course](https://sites.google.com/view/uczeniezewzmocnieniem/home).


Previously, I was leading a risk modeling team at [KNF](www.knf.gov.pl/en/) for
multiple years.
I contributed to the implementation of Solvency II in Poland and the European Union,
was a member of EIOPA working groups, and took part in
colleges of supervisors for major insurance EU groups.
I am also a charted actuary.


<!--

in particular I co-authored of the [Integration Technique 2](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32015R0035) 
for partial internal models,
an SDP-based algorithm described in Solvency II Delegated Regulation.

I graduated from the [University of Warsaw](https://www.mimuw.edu.pl/en"),
and completed my Ph.D. in stochastic optimal control at [IM
PAN](www.impan.pl/en).

He is a co-author of the [Integration Technique 2](https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32015R0035) for partial internal models,
an SDP-based algorithm described in Solvency II Delegated Regulation, and is a chartered actuary.

Łukasz Kuciński co-founded AWARElab in 2018, after careers in academia and risk modeling.
He graduated from the [University of Warsaw](https://www.mimuw.edu.pl/en"),
completed his Ph.D. (with honors) in stochastic optimal control at [IM
PAN](www.impan.pl/en),
and
--->


<!--- TODO[PM]: would be good to add CV -->
