---
# Display name
title: Mateusz Olko

# Username (this should match the folder name)
authors:
- mateusz-olko
weight: 20

# Is this the primary user of the site?
superuser: true

# Role/position
role: Ph.D. Student

# Organizations/Affiliations
organizations:
- name: University of Warsaw
  url: ""

# Short bio (displayed in user profile at end of posts)
bio: My research interests include TODO

interests:
- Reinforcement Learning
- Representation Learning

education:
  courses:
  - course: Master in Computer Science
    institution: University of Warsaw
    year: 2021
  - course: Bachelor in Computer Science
    institution: University of Warsaw
    year: 2019

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:mo382777@students.mimuw.edu.pl"
# - icon: github
#   icon_pack: fab
#   link: https://github.com/do-not-be-hasty
# - icon: cv
#   icon_pack: ai
#   link: https://students.mimuw.edu.pl/~mz371908/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Ph.D. Students
---


I am a Ph.D. student of Computer Science at the Warsaw Doctoral School of
Mathematics and Computer Science, jointly run by the [University of
Warsaw](https://www.uw.edu.pl/) and the [Institute of Mathematics of the Polish
Academy of Sciences](https://www.impan.pl/). My advisor is [prof. Piotr Miłoś]({{% ref "/content/authors/piotr-miłos/_index.md" %}}).

My Master's thesis was a part of a research project focused on applying Bayesian methods for improved exploration in model-free RL.

Before my Ph.D. studies, I have been applying deep learning in various areas NLP, Computer Vision, sound classification, and generation. I gained my practical experience working in Samsung Research Poland and TCL Research Europe.

