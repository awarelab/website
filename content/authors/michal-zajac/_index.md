---
# Display name
title: Michał Zając

# Username (this should match the folder name)
authors:
- michal-zajac
weight: 23

# Is this the primary user of the site?
superuser: true

# Role/position
role: Ph.D. Student

# Organizations/Affiliations
organizations:
 - name: Jagiellonian University
   url: ""

# Short bio (displayed in user profile at end of posts)
bio: ""

interests:
- Machine Learning
- Reinforcement Learning
- Continual Learning

education:
  courses:
  - course: MSc in Computer Science
    institution: Jagiellonian University
    year: 2017
  - course: BSc in Computer Science
    institution: Jagiellonian University
    year: 2015
  - course: BSc in Mathematics
    institution: Jagiellonian University
    year: 2014

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: envelope
  icon_pack: fas
  link: "mailto:mzajac@ii.uj.edu.pl"
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.pl/citations?user=5HHtXzwAAAAJ
- icon: cv
  icon_pack: ai
  link: https://drive.google.com/file/d/1CD93AI0ItRhVOm3fPRu9F5UG9sKilu-s/view

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
- Ph.D. Students
---


I am a Ph.D. student at Jagiellonian University, advised by
[prof. Piotr Miłoś]({{% ref "/content/authors/piotr-miłos/_index.md" %}}).
I am interested in machine learning and reinforcement learning.

During my Ph.D. studies, I was an intern at Google Brain Zurich, developing
Google Research Football environment. Currently I work at Allegro Machine Learning Research
on content optimization for e-commerce. Previously I worked also at Nomagic,
developing deep learning-based robotic solutions for warehouse automation.
